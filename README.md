PLAYWIRE
===================
INTRODUCTION:
----------------------------
Playwire module will help to add the playwire type video in the content type.

REQUIREMENTS:
----------------------------
Playwire App ID.

INSTALLATION:
----------------------------
1. Enable the module playwire.
2. Add the playwire app id using the configuration page.
3. Add the PlayWire Video ID field in the content type.

Note: If you are not adding any playwire app id than by default it is "1" so
some public video will be shown.

CONFIGURATION:
----------------------------
Playwire module have below configuration setting page
1. Add the Playwire App ID using below config page
/admin/config/services/playwire/configuration

Note: If you are changing the playwire app id than you need to clear the
drupal cache.
